class ErrorModel {
  
  String message;

  ErrorModel(this.message);

  ErrorModel.fromJson(Map<String, dynamic> map) : message = map['message'];

}