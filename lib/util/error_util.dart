import 'dart:convert';

import 'package:flutter_login/model/error_model.dart';
import 'package:http/http.dart';

class ErrorUtil {

  static String message(Response response) {
    if (response.statusCode == 400) {
      var error = ErrorModel.fromJson( json.decode(response.body) );
      return error.message;
    }
    return "Terjadi kesalahan";
  }
}