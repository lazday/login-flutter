
class ApiUtil {
  // static final String URL = "http://192.168.1.4/api/user/";
  static final String URL = "https://demo.lazday.com/api/user/";
  static Uri baseUrl (String endpoint) {
    return Uri.parse( URL + endpoint );
  }
}