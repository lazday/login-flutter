import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login/model/error_model.dart';
import 'package:flutter_login/model/login_model.dart';
import 'package:flutter_login/ui/home.dart';
import 'package:flutter_login/ui/register.dart';
import 'package:flutter_login/util/api_util.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_login/preferences/pref_manager.dart' as pref;

/// LOGIN TEST
/// irsyad@lazday.com 12345

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  String email = "";
  String password = "";
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding (
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView (
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Text(
                    'Sign In',
                    style: TextStyle(fontSize: 38, color: Colors.black45, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 5, right: 5, top: 20),
                child: TextFormField(
                  style: TextStyle(fontSize: 18),
                  decoration: InputDecoration (
                    border: UnderlineInputBorder (),
                    labelText: 'Your Email',
                  ),
                  onChanged: (String text){
                    email = text;
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 5, right: 5, top: 10),
                child: TextFormField(
                  obscureText: true,
                  style: TextStyle(fontSize: 18),
                  decoration: InputDecoration (
                    border: UnderlineInputBorder (),
                    labelText: 'Password',
                  ),
                  onChanged: (String text){
                    password = text;
                  },
                ),
              ),
              Container (
                margin: EdgeInsets.only(top: 30),
                child: isLoading ? setLoading() : setButton(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.all(5),
                    child: Text(
                      "Don't have account??",
                      style: TextStyle(fontSize: 16, color: Colors.black54),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.all(5),
                    child: InkWell(
                      child: Text(
                        'Sign Up',
                        style: TextStyle(fontSize: 16, color: Colors.blueAccent, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.left,
                      ),
                      onTap: (){
                        Navigator.of(context) .pushReplacement(
                            MaterialPageRoute(builder: (BuildContext context) => Register())
                        );
                      },
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget setButton(){
    return Container(
      width: double.infinity,
      child: MaterialButton(
          height: 50,
          color: Colors.blueAccent,
          shape: RoundedRectangleBorder (
            borderRadius: BorderRadius.circular(20),
          ),
          child: Text('Sign In', style: TextStyle (color: Colors.white, fontSize: 18),),
          onPressed: () {
            if(isRequired()) login(context);
          }
      )
    );
  }

  Widget setLoading(){
    return Padding(
        padding: EdgeInsets.all(10),
        child: Container (
          width: 50,
          height: 50,
          child: CircularProgressIndicator(),
        ),
    );
  }

  bool isRequired(){
    if (email.isEmpty) {
      Fluttertoast.showToast(msg: 'Email is required');
      return false;
    } else if (password.isEmpty) {
      Fluttertoast.showToast(msg: 'Password is required');
      return false;
    }
    return true;
  }

  login(BuildContext context) async {
    setState(() => isLoading = true );

    final response = await http.get(
      ApiUtil.baseUrl( "login.php?email=$email&password=$password" )
    );
    var result = jsonDecode( response.body );
    if (response.statusCode == 200) {
      setState(() => isLoading = false );

      pref.PrefManager.setIsLogin(1);
      pref.PrefManager.setUserData(response.body);

      var login = LoginModel.fromJson(result);
      Fluttertoast.showToast(msg: login.message);
      Navigator.of(context) .pushReplacement(
          MaterialPageRoute(builder: (BuildContext context) => Home())
      );

    } else {
      setState(() => isLoading = false );
      if (response.statusCode == 400) {
        var error = ErrorModel.fromJson(result);
        Fluttertoast.showToast(msg: error.message);
      } else {
        Fluttertoast.showToast(msg: 'Terjadi kesalahan');
      }
    }

  }

}
