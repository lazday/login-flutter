import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login/model/login_model.dart';
import 'package:flutter_login/model/user_model.dart';
import 'package:flutter_login/preferences/pref_manager.dart' as pref;
import 'package:flutter_login/ui/login.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  String name = "";
  String email = "";

  @override
  void initState() {
    getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar (title: Text('Login'),),
      body: Padding (
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.all(10),
              child: Text(
                email,
                style: TextStyle(fontSize: 18, color: Colors.black54),
                textAlign: TextAlign.center,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: Text(
                    "Login as ",
                    style: TextStyle(fontSize: 24, color: Colors.black54),
                    textAlign: TextAlign.left,
                  ),
                ),
                Container(
                  child: Text(
                    name ,
                    style: TextStyle(fontSize: 24, color: Colors.black45, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                )
              ],
            ),
            Container (
                margin: EdgeInsets.only(top: 20),
                width: double.infinity,
                child: MaterialButton(
                    height: 50,
                    color: Colors.white,
                    shape: RoundedRectangleBorder (
                      side: BorderSide(color: Colors.blueAccent, width: 2, style: BorderStyle.solid),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text('Sign Out', style: TextStyle (color: Colors.blueAccent, fontSize: 18),),
                    onPressed: () {
                      pref.PrefManager.logout().then((value) {
                        Navigator.of(context) .pushReplacement(
                            MaterialPageRoute(builder: (BuildContext context) => Login())
                        );
                      });
                    }
                )
            ),
          ],
        ),
      ),
    );
  }

  getUser() {
    pref.PrefManager.getUserData.then((value) {
      LoginModel user = LoginModel.fromJson(jsonDecode(value));
      setState(() {
        name = user.data.name;
        email = user.data.email;
      });
    });
  }
}
